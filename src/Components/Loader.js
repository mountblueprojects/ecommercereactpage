import React from "react";
// import "../styles/Loader.css";

class Loader extends React.Component {
  constructor(props) {
    super(props);
    this.state = { clock: "" };
  }

  interval = (myFunc) => {
    setInterval(myFunc, 125);
  };

  load = () => {
    let arr = ["🕐", "🕜", "🕑", "🕝", "🕒", "🕞"];
    this.interval(() => {
      this.setState({
        clock: arr[Math.floor(Math.random() * arr.length)],
      });
    });
  };

  componentDidMount() {
    this.load();
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <div className="preload">
        <div className="emoji">{this.state.clock}</div>
      </div>
    );
  }
}

export default Loader;
