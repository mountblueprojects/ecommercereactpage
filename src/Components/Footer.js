import React from "react";
import "../styles/Footer.css";
import facebook from "../images/icon-facebook.svg";
import youtube from "../images/icon-youtube.svg";
import twitter from "../images/icon-twitter.svg";
import pinterest from "../images/icon-pinterest.svg";
import instagram from "../images/icon-instagram.svg";

class Footer extends React.Component {
  render() {
    return (
      <div className="footer">
        <div className="allOptions">
          <div className="logos">
            <div>Follow Us:</div>
            <img src={facebook} alt="fb" />
            <img src={youtube} alt="yt" />
            <img src={twitter} alt="tw" />
            <img src={pinterest} alt="pt" />
            <img src={instagram} alt="ig" />
          </div>

          <div id="credits">
            <p>© ShopHub All Rights Reserved</p>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
