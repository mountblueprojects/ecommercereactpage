import React from "react";
import "./App.css";
import Home from "./Pages/Home";
import Cart from "./Pages/Cart";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import Product from "./Pages/Product";
import ErrorPage from "./Pages/ErrorPage";
import axios from "axios";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      loader: false,
      error: false,
      editor: false,
      product: {},
      close: false,
      formError: false,
      popup: false,
      confirmPopup: false,
      cart: [],
      emptyCart: true,
      quantity: {},
      cartConfirm: false,
    };
  }

  componentDidMount() {
    this.setState({ loader: true });
    axios({
      url: "products",
      baseURL: "https://fakestoreapi.com",
      method: "get",
    })
      .then((response) => {
        this.setState({ products: response.data });
      })
      .catch((error) => {
        this.setState({ error: true });
      })
      .finally(() => {
        this.setState({ loader: false });
      });
  }

  handleClick = (event) => {
    if (event.target.id === "close") {
      this.setState({
        popup: true,
      });
    } else if (event.target.id === "yes") {
      this.setState({ editor: false, popup: false, close: false });
    } else if (event.target.id === "no") {
      this.setState({ popup: false });
    } else if (event.target.id === "ok") {
      this.setState({ confirmPopup: false });
    } else {
      this.setState({
        product: this.state.products.filter((product) => {
          return (
            product.id ===
            parseInt(event.target.parentElement.className.split(" ")[1])
          );
        })[0],
        editor: true,
        close: true,
      });
    }
  };

  onChange = (event) => {
    const property = event.target.id;
    if (property.split(".").length === 1) {
      this.setState({
        product: { ...this.state.product, [property]: event.target.value },
      });
    } else {
      const rating = property.split(".")[0];
      const ratingProperty = property.split(".")[1];
      this.setState({
        product: {
          ...this.state.product,
          [rating]: {
            ...this.state.product.rating,
            [ratingProperty]: event.target.value,
          },
        },
      });
    }
  };

  onSubmit = (event) => {
    event.preventDefault();
    if (
      Object.values(this.state.product).includes("") ||
      Object.values(this.state.product.rating).includes("")
    ) {
      this.setState({ formError: true });
      return;
    } else {
      this.setState({
        products: this.state.products.map((product) => {
          if (product.id === this.state.product.id) {
            product = this.state.product;
          }
          return product;
        }),
        confirmPopup: true,
        editor: false,
        close: false,
      });
    }
  };

  addToCart = (event) => {
    const product = parseInt(
      event.target.parentElement.className.split(" ")[1]
    );
    if (event.target.className.split(" ")[0] !== "add") {
      this.setState({ cartConfirm: true });
    }
    if (this.state.quantity.hasOwnProperty(product)) {
      if (this.state.quantity[product] === 0) {
        this.setState({
          cart: [...this.state.cart, product],
          emptyCart: false,
          quantity: { ...this.state.quantity, [product]: 1 },
        });
      }
      this.setState({
        quantity: {
          ...this.state.quantity,
          [product]: this.state.quantity[product] + 1,
        },
      });
    } else {
      this.setState({
        cart: [...this.state.cart, product],
        emptyCart: false,
        quantity: { ...this.state.quantity, [product]: 1 },
      });
    }
    setTimeout(() => {
      this.setState({ cartConfirm: false });
    }, 1000);
  };

  removeItem = (event) => {
    const product = event.target.className.split(" ")[1];
    const productIndex = this.state.cart.findIndex((id) => {
      return id === parseInt(product);
    });

    if (this.state.cart.length === 1) {
      this.setState({ emptyCart: true });
    }
    if (this.state.quantity[product] === 1) {
      this.setState((state) => {
        return {
          cart: [
            ...state.cart.slice(0, productIndex),
            ...state.cart.slice(productIndex + 1, state.cart.length),
          ],
          quantity: {
            ...this.state.quantity,
            [product]: this.state.quantity[product] - 1,
          },
        };
      });
    } else {
      this.setState({
        quantity: {
          ...this.state.quantity,
          [product]: this.state.quantity[product] - 1,
        },
      });
    }
  };

  render() {
    return (
      <BrowserRouter>
        <Header isEmpty={this.state.emptyCart} />
        {this.state.cartConfirm && (
          <div className="cartPopup">
            <p>Product Added to Cart!</p>
          </div>
        )}
        <Switch>
          <Route path="/" exact>
            <Home
              state={this.state}
              handleClick={this.handleClick}
              onChange={this.onChange}
              onSubmit={this.onSubmit}
              addToCart={this.addToCart}
            />
          </Route>
          <Route path="/cart" exact>
            <Cart
              state={this.state}
              removeItem={this.removeItem}
              addToCart={this.addToCart}
            />
          </Route>
          <Route
            path="/product:id"
            render={(props) => (
              <Product {...props} addToCart={this.addToCart} />
            )}
          />
          <Route path="*" component={ErrorPage} />
        </Switch>
        <Footer />
      </BrowserRouter>
    );
  }
}

export default App;
