import React from "react";
import "../styles/Article.css";
import UpperContent from "./UpperContent";
import CardsContainer from "./CardsContainer";
import Loader from "./Loader";

class Article extends React.Component {
  render() {
    return (
      <div className="article">
        <UpperContent />
        {this.props.state.error && <h1>An Error has occurred</h1>}
        {this.props.state.products.length === 0 &&
          !this.props.state.loader &&
          !this.props.state.error && <h1>No Products Found</h1>}
        {this.props.state.loader && <Loader />}

        {!this.props.state.error && this.props.state.products.length && (
          <CardsContainer
            state={this.props.state}
            product={this.props.state.product}
            handleClick={this.props.handleClick}
            onChange={this.props.onChange}
            onSubmit={this.props.onSubmit}
            addToCart={this.props.addToCart}
          />
        )}
      </div>
    );
  }
}

export default Article;
