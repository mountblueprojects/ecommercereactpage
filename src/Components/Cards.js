import React from "react";
import SingleCard from "./SingleCard";

class Cards extends React.Component {
  render() {
    return (
      <section className="cards">
        {this.props.state.products.length !== 0 && !this.props.state.error && (
          <SingleCard
            products={this.props.state.products}
            handleClick={this.props.handleClick}
            addToCart={this.props.addToCart}
            state={this.props.state}
          />
        )}
      </section>
    );
  }
}

export default Cards;
