import React from "react";
import Cards from "./Cards";
import EditCard from "./EditCard";

export class CardsContainer extends React.Component {
  render() {
    return (
      <div id="cardsContainer">
        <Cards
          state={this.props.state}
          handleClick={this.props.handleClick}
          addToCart={this.props.addToCart}
        />
        <EditCard
          state={this.props.state}
          product={this.props.product}
          onChange={this.props.onChange}
          onSubmit={this.props.onSubmit}
          handleClick={this.props.handleClick}
        />
      </div>
    );
  }
}

export default CardsContainer;
