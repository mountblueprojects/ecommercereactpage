import image1 from "../images/zoom.png";
import image2 from "../images/8109.png";

import React, { Component } from "react";
import { Link } from "react-router-dom";

export class SingleCard extends Component {
  render() {
    return this.props.products.map((product) => {
      return (
        <div key={product.id} className={`card ${product.id}`}>
          <div
            className={`options ${product.id}`}
            onClick={this.props.handleClick}
          >
            <img className="zoom" src={image1} alt="zoom" />
            <img className="bag" src={image2} alt="bag" />
          </div>

          <div className={`product ${product.id}`}>
            <img
              src={product.image}
              alt="Product"
              onClick={this.props.handleClick}
            />
          </div>

          <div
            className={`details ${product.id}`}
            onClick={this.props.handleClick}
          >
            <div className="name" onClick={this.props.handleClick}>
              {product.title}
            </div>
            <br />
            <div className="price" onClick={this.props.handleClick}>
              ${product.price}
            </div>
            <br />
            <div className="description" onClick={this.props.handleClick}>
              {product.description}
            </div>
            <br />
            <div className="category" onClick={this.props.handleClick}>
              {product.category}
            </div>
            <br />
            <div className="rating" onClick={this.props.handleClick}>
              <i className="fa-solid fa-star"></i>
              {product.rating.rate} ({product.rating.count})
            </div>
          </div>

          <div className={`functions ${product.id}`}>
            <Link
              to={{
                pathname: `/product:${product.id}`,
              }}
            >
              <button id="view">View</button>
            </Link>
            <button id="buy" onClick={this.props.addToCart}>
              Add to Cart
            </button>
          </div>
        </div>
      );
    });
  }
}

export default SingleCard;
