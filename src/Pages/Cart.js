import React from "react";
import PriceDetails from "../Components/PriceDetails";
import Item from "../Components/Item";
import "../styles/Cart.css";

class Cart extends React.Component {
  render() {
    return (
      <>
        {this.props.state.cart.length === 0 && (
          <div id="emptyCart">
            <h1>Cart is Empty!</h1>
          </div>
        )}
        {this.props.state.cart.length !== 0 && (
          <div id="cart">
            <div id="items">
              <Item
                state={this.props.state}
                removeItem={this.props.removeItem}
                addToCart={this.props.addToCart}
              />
            </div>
            <PriceDetails state={this.props.state} />
          </div>
        )}
      </>
    );
  }
}

export default Cart;
