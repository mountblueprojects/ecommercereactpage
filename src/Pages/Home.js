import React from "react";
import Article from "../Components/Article";

class Home extends React.Component {
  render() {
    return (
      <Article
        state={this.props.state}
        handleClick={this.props.handleClick}
        onChange={this.props.onChange}
        onSubmit={this.props.onSubmit}
        addToCart={this.props.addToCart}
      />
    );
  }
}

export default Home;
