import React, { Component } from "react";

export class Item extends Component {
  render() {
    return this.props.state.products
      .filter((product) => {
        return this.props.state.cart.includes(product.id);
      })
      .map((product) => {
        return (
          <div className="item">
            <img src={`${product.image}`} alt="Product" />
            <div className="finalDetails">
              <div className="finalTitle">{`${product.title}`}</div>
              <div className="finalPrice">${`${product.price}`}</div>
              <div className={`modify ${product.id}`}>
                <button
                  className={`add ${product.id}`}
                  onClick={this.props.addToCart}
                >
                  Add
                </button>
                <div className="number">
                  {this.props.state.quantity[product.id]}
                </div>
                <button
                  className={`remove ${product.id}`}
                  onClick={this.props.removeItem}
                >
                  Remove
                </button>
              </div>
            </div>
          </div>
        );
      });
  }
}

export default Item;
