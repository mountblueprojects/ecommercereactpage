import React from "react";
import image from "../images/ecommerce-website-2-483993.webp";
import "../styles/Header.css";
import { Link } from "react-router-dom";

class Header extends React.Component {
  render() {
    return (
      <div className="header container">
        <img id="logo" src={image} alt="logo" />
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/">Shop</Link>
          </li>
          <Link to="/cart">
            <div className="cart">
              {this.props.isEmpty && <i class="fa-solid fa-cart-shopping"></i>}
              {!this.props.isEmpty && <i class="fa-solid fa-cart-plus"></i>}
            </div>
          </Link>
        </ul>
      </div>
    );
  }
}

export default Header;
