import React, { Component } from "react";

export class PriceDetails extends Component {
  render() {
    return (
      <div className="priceDetails">
        <h4>PRICE DETAILS</h4>

        <div className="detailField">
          <div>Total Price</div>
          <div>
            $
            {this.props.state.products
              .filter((product) => {
                return this.props.state.cart.includes(product.id);
              })
              .reduce((accumulator, current) => {
                accumulator +=
                  current.price * this.props.state.quantity[current.id];
                return accumulator;
              }, 0)
              .toFixed(2)}
          </div>
        </div>

        <div className="detailField">
          <div>Delivery Charges</div>
          <div>$5</div>
        </div>

        <div className="detailField">
          <div>Total Amount</div>
          <div>
            $
            {this.props.state.products
              .filter((product) => {
                return this.props.state.cart.includes(product.id);
              })
              .reduce((accumulator, current) => {
                accumulator +=
                  current.price * this.props.state.quantity[current.id];
                return accumulator;
              }, 5)
              .toFixed(2)}
          </div>
        </div>
      </div>
    );
  }
}

export default PriceDetails;
