import React from "react";
import axios from "axios";
import "../styles/Product.css";
import Loader from "../Components/Loader";
import { Link } from "react-router-dom";

class Product extends React.Component {
  constructor(props) {
    super(props);
    this.state = { product: null, loader: true };
  }

  componentDidMount() {
    axios({
      url: `products/${this.props.match.params.id.split(":")[1]}`,
      baseURL: "https://fakestoreapi.com",
      method: "get",
    })
      .then((response) => {
        this.setState({ product: response.data });
      })
      .catch((error) => {
        this.setState({ error: true });
      })
      .finally(() => {
        this.setState({ loader: false });
      });
  }

  render() {
    return (
      <>
        {this.state.loader && <Loader />}

        {!this.state.loader && (
          <>
            <div className="back">
              <Link to="/">
                <i className="fa-solid fa-arrow-left-long"></i>
              </Link>
            </div>

            <div className="singleProduct">
              <div className="productImage">
                <img src={`${this.state.product.image}`} alt="Product" />
              </div>
              <div className={`productDetails ${this.state.product.id}`}>
                <div className="productName">{this.state.product.title}</div>
                <div className="productPrice">${this.state.product.price}</div>
                <div className="productDescription">
                  {this.state.product.description}
                </div>
                <div>
                  <i className="fa-solid fa-star"></i>
                  {this.state.product.rating.rate} (
                  {this.state.product.rating.count})
                </div>
                <button id="singleBuy" onClick={this.props.addToCart}>
                  Add to Cart
                </button>
              </div>
            </div>
          </>
        )}
      </>
    );
  }
}

export default Product;
