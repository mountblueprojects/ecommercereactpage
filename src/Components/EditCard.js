import React from "react";

export class EditCard extends React.Component {
  render() {
    return (
      <section id="editSection">
        <div id="edit">
          <div id="editTitle">
            <h1>Edit Product</h1>
            {this.props.state.close && (
              <h5 id="close" onClick={this.props.handleClick}>
                X
              </h5>
            )}
          </div>
          {this.props.state.popup && (
            <div className="popup">
              <div className="confirm">
                <p>Are you sure you want to exit?</p>
                <p>The data will be lost</p>
                <div className="buttons">
                  <button id="yes" onClick={this.props.handleClick}>
                    Yes
                  </button>
                  <button id="no" onClick={this.props.handleClick}>
                    No
                  </button>
                </div>
              </div>
            </div>
          )}

          {this.props.state.confirmPopup && (
            <div className="popup">
              <div className="confirm">
                <p>Updated Successfully!!</p>
                <div className="buttons">
                  <button id="ok" onClick={this.props.handleClick}>
                    OK
                  </button>
                </div>
              </div>
            </div>
          )}

          {!this.props.state.editor && <h3>Click on a Product to Edit here</h3>}
          {this.props.state.editor && (
            <form onSubmit={this.props.onSubmit}>
              <div className="formElement">
                <div className="field">
                  <label>Image: </label>
                  <input
                    id="image"
                    type="text"
                    value={this.props.product.image}
                    onChange={this.props.onChange}
                  ></input>
                </div>
                {this.props.state.formError && !this.props.product.image && (
                  <p>Please enter image url</p>
                )}
              </div>

              <div className="formElement">
                <div className="field">
                  <label>Title: </label>
                  <input
                    id="title"
                    type="text"
                    value={this.props.product.title}
                    onChange={this.props.onChange}
                  ></input>
                </div>
                {this.props.state.formError && !this.props.product.title && (
                  <p>Please enter a title</p>
                )}
              </div>

              <div className="formElement">
                <div className="field">
                  <label>Price: </label>
                  <input
                    id="price"
                    type="number"
                    value={this.props.product.price}
                    onChange={this.props.onChange}
                  ></input>
                </div>
                {this.props.state.formError && !this.props.product.price && (
                  <p>Please enter a price</p>
                )}
              </div>

              <div className="formElement">
                <div className="field">
                  <label>Description: </label>
                  <textarea
                    id="description"
                    type="text"
                    value={this.props.product.description}
                    onChange={this.props.onChange}
                  ></textarea>
                </div>
                {this.props.state.formError &&
                  !this.props.product.description && (
                    <p>Please enter a description</p>
                  )}
              </div>

              <div className="formElement">
                <div className="field">
                  <label>Category: </label>
                  <input
                    id="category"
                    type="text"
                    value={this.props.product.category}
                    onChange={this.props.onChange}
                  ></input>
                </div>
                {this.props.state.formError && !this.props.product.category && (
                  <p>Please enter a category</p>
                )}
              </div>

              <div className="formElement">
                <div className="field">
                  <label>Rating: </label>
                  <input
                    id="rating.rate"
                    type="number"
                    value={this.props.product.rating.rate}
                    onChange={this.props.onChange}
                  ></input>
                </div>
                {this.props.state.formError &&
                  !this.props.product.rating.rate && (
                    <p>Please enter a rating</p>
                  )}
              </div>

              <div className="formElement">
                <div className="field">
                  <label>Count: </label>
                  <input
                    id="rating.count"
                    type="number"
                    value={this.props.product.rating.count}
                    onChange={this.props.onChange}
                  ></input>
                </div>
                {this.props.state.formError &&
                  !this.props.product.rating.count && (
                    <p className="formError">Please enter a count</p>
                  )}
              </div>

              <input type="submit" value="Update Details" id="submit"></input>
            </form>
          )}
        </div>
      </section>
    );
  }
}

export default EditCard;
